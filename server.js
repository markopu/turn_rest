var cluster = require('cluster');
var https = require('https');
var url = require('url');
var crypto = require('crypto');
var config = require('config');
var pg = require('pg');
var jwt = require('jsonwebtoken');
var fs = require('fs');

var db_config = config.get('db_config');
var server_port = config.get('server').port;
var server_listen_address = config.get('server').address;
var secret_key = config.get('server').secret_key;
var turn_servers = config.get('server').turn_servers;
var stun_servers = config.get('server').stun_servers;
var key_location = config.get('server').key;
var cert_location = config.get('server').cert;
var num_cpus = config.get('server').num_cpus;
var server_options = {
    key: fs.readFileSync(key_location),
    cert: fs.readFileSync(cert_location)
};
var allowed_origin = config.get('server').allowed_origin;

var connection_string = "postgres://" + db_config.user + ':' +
    db_config.password + '@' + db_config.host + ':' + db_config.port + '/' + db_config.database;

/**
* Get user's password by issuing the following SQL query
* SELECT value FROM prosody WHERE user='<user_name>' AND `key`='password';
* @function getUserPassword
* @param {string} username Username
* @param {function} gotPassword Got password callback
*/
function getUserPassword(connection_string, username, gotPassword, invalidUser) {
    pg.connect(connection_string, function(err, client, done) {
	var password;
	var sql = 'SELECT value FROM prosody WHERE "user"=\'' + username + '\' AND "key"=\'password\'';

	if (err) {
	    return console.log(err);
	}

	client.query(sql, function (err, result) {
	    done();

	    if (err) {
		console.log(err);
	    }
	    
	    if (typeof result.rows[0] === 'undefined') {
		invalidUser();
	    } else {
		password = result.rows[0].value;
		gotPassword(password);
	    }
	});
    });
}

/**
 * Send response in form of plain text or html data.
 * @function sendData
 * @param {Object} res Http server response object.
 * @param {string} data Data to send.
 */
function sendData(res, data) {
    res.setHeader('Content-Type', 'application/json');
    res.setHeader('Access-Control-Allow-Origin',
		  allowed_origin);
    res.writeHead(res.statusCode);
    res.write(data);
    res.end();
}

/**
 * Send text response with given status code
 * @function sendTextResponse
 * @param {Object} res Http response object.
 * @param {integer} code Http code to send
 * @param {string} text String to send
 */
function sendTextResponse(res, code, text) {
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Access-Control-Allow-Origin',
		  allowed_origin);
    res.setHeader('Access-Control-Allow-Headers',
		  'authorization, content-type');
    res.writeHead(code);
    res.write(text);
    res.end();
}

/**
 * Generate TURN server configuration based on use of ephemeral
 * credentials.
 * @function genTURNConfiguration
 * @param {string} username Username to generate configuration with.
 * @param {string[]} turn_servers List of all possible connections to 
 * turn server.
 * @param {string[]} stun_servers List of stun servers.
 */
function genTURNConfiguration(username, turn_servers, stun_servers) {
    var ttl = 86400;
    var seconds = parseInt(Date.now() / 1000) + ttl;
    var temporary_username = seconds.toString() + ':' + username;
    var hmac = crypto.createHmac('sha1', secret_key);
    
    hmac.setEncoding('base64');
    hmac.write(temporary_username);
    hmac.end();

    return {
	username: temporary_username,
	password: hmac.read(),
	ttl: ttl,
	uris: turn_servers,
	stun_uris: stun_servers
    };
}

/**
 * Verify JSON Web Token and execute callback if token is verified
 * correctly
 * @function verifyJWTToken
 * @param {string} token JWT token to verify
 * @param {string} secret_key key to verify token against
 * @param {Object} res Response object.
 * @param {function} tokenOk callback to execute on successful
 * token verification
 */ 
function verifyJWTToken(token, secret_key, res, tokenOk) {
    if (typeof token !== 'undefined') {
	token = token.substr(token.indexOf(" ") + 1);

	jwt.verify(token, secret_key, {}, function(err, token) {
	    if (err === null) {
		    tokenOk();
	    } else {
		sendTextResponse(res, 401, '401 Unauthorized');
	    }
	});
    } else {
	sendTextResponse(res, 401, '401 Unauthorized');
    }
}

if (cluster.isMaster) {
    // Create workers
    for (var i = 0; i < num_cpus; i++) {
	cluster.fork();
    }

    // If worker dies re-start it
    cluster.on('exit', function(worker, code, signal) {
	console.log('Worker ' + worker.id + ' died.');
	cluster.fork();
    });
} else {
    https.createServer(server_options, function(req, res) {
	console.log(res.statusCode, req.method, req.url);
	var full_body = '';
	
	switch (req.url) {
	case '/jwt':
	    if (req.method === 'POST') {
		req.on('data', function(chunk) {
		    full_body += chunk.toString();
		});

		req.on('end', function() {
		    var credentials;

		    function gotPassword(password) {
			// check if passwords match
			if (credentials.password === password) {
			    // Generate JSON web token
			    var token = jwt.sign({name: credentials.username}, secret_key);
			    sendData(res, token);
			} else {
			    sendTextResponse(res, 401, '401 Unauthorized');
			}
		    }

		    function invalidUser() {
			sendTextResponse(res, 401, '401 Unauthorized');
		    }
		    
		    try {
			credentials = JSON.parse(full_body);
			
			getUserPassword(connection_string,
					credentials.username,
					gotPassword, invalidUser);
		    } catch (err) {
			sendTextResponse(res, 400, '400 Bad request');
		    }
		});
	    } else {
		sendTextResponse(res, 403, '403 Forbidden');
	    }
	    break;
	case '/turn_credentials':
	    if (req.method === 'POST') {
		req.on('data', function(chunk) {
		    full_body += chunk.toString();
		});
		
		req.on('end', function() {
		    var token = req.headers.authorization;

		    verifyJWTToken(token, secret_key, res, function() {
			try {
			    var user_data = JSON.parse(full_body);
			    var configuration = genTURNConfiguration(user_data.username,
								     turn_servers,
								     stun_servers);
			    
			    sendData(res, JSON.stringify(configuration));
			} catch (err) {
			    sendTextResponse(res, 400, '400 Bad request');
			}
		    });
		});
	    } else if (req.method === 'OPTIONS') {
		sendTextResponse(res, 200, '');
	    } else {
		sendTextResponse(res, 403, '403 Forbidden');
	    }
	    break;
	default:
	    sendData(res, 'TURN REST credentials and authorization server.');
	    break;
	}
    }).listen(server_port, server_listen_address);
    console.log('Server running on', server_listen_address + ':' + server_port + '.');
}


