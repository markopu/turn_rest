# ** turn_rest ** #

Node.js based ephemeral credentials server, based on https://tools.ietf.org/html/draft-uberti-rtcweb-turn-rest-00.

## Install ##

```
#!bash

git clone https://markopu@bitbucket.org/markopu/turn_rest.git
cd turn_rest
npm install
```

## Configuration ##
Edit config/default.json file. Be sure to provide your own SSL certificate in certs/folder.

## Running ##
```
#!bash
node server.js
```

## Using the REST API ##
To get ephemeral credentials for TURN server first send a POST request to get JSON Web Token, which is used later for authorization:


```
#!javascript

var xhr = new XMLHttpRequest();
xhr.open('POST', '/jwt', true);
xhr.send('{"username": <username>, "password": <password>}');
```

On successful request, store the token in the local storage.

To get ephemeral credentials issue new POST request with JWT in the Authorization header, to the server with URL '/turn_credentials'.

```
#!javascript

var xhr = new XMLHttpRequest();
xhr.open('POST', '/jwt', true);
xhr.setRequestHeader('Authorization', 'Bearer ' + <JWT>);
xhr.send('{"username": <username>, "password": <password>}');
```